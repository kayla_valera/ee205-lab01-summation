///////////////////////////////////////////////////////////////////////////////
// University of Hawaii, College of Engineering
// EE 205 - Object Oriented Programming
// Lab 01d - Summation
//
// Usage:  summation n
//   n:  Sum the digits from 1 to n
//
// Result:
//   The sum of the digits from 1 to n
//
// Example:
//   $ summation 6
//   The sum of the digits from 1 to 6 is 21
//
// @author Kayla Valera <kvalera@hawaii.edu>>
// @date   1_19_2021y
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char* argv[]) {
   int n = atoi(argv[1]);
   int i,m;
   	for (i=1;i<=n; i=i+1){
	m = m + i;
}
 printf("The sum of the digits from 1 to %d is %d\n", n, m); 
   return 0;
}
